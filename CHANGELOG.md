# Changelog

## 1.0.2 (2019-08-20)
- Adjusted placement of card image border to be attached to image instead of image container
- Fixed social share icon link color on hover. Previously had been set to white, now inherits from general link color and general link hover color. 

## 1.0.1 (2019-08-15)
- Updated support e-mail

## 1.0.0 (2019-08-13)
- First publish ready version of Roots

## 0.1.3 (2019-08-08)
- Updated config.json meta information

## 0.1.2 (2019-07-24)
- Adjusted default logo size
- Fixed 'as uploaded' logo option bug where logo didn't line up with left side of nav

## 0.1.1 (2019-07-9)
- Added theme editor setting for text logo font size on mobile
- Added ellipsis on text logo on mobile to prevent overlap on menu and cart logos
- Fixed logo from getting cut off when "optimized for theme" sizing selected

## 0.1.0 (2019-06-27)
- General testing and refinements
- First official version for review

## 0.0.0
- Initial version of Roots for review
